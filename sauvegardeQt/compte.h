#ifndef COMPTE_H
#define COMPTE_H

#ifndef COMPTE_H_INCLUDED
#define COMPTE_H_INCLUDED
#include <iostream>
#include <string>
#include<vector>
#include <QString>

using namespace std;

class CompteVirtuel;
class CompteReel;
class Transaction;

class Compte{
protected:
    QString typeCompte;
    CompteVirtuel* comptePere;
    double solde;
    int id;
    int id_pere;
    const QString nomCompte;
public:
    Compte(const QString type, CompteVirtuel* cp, double s):typeCompte(type),comptePere(cp), solde(s){}
    Compte(const QString type, double s, int id1, int id_pere1, const QString nom):typeCompte(type), solde(s), id(id1), id_pere(id_pere1), nomCompte(nom){}
    const QString & getTypeCompte() const{return typeCompte;}

    CompteVirtuel* getComptePere() const{return comptePere;}

    const double & getSolde() const{return solde;}

    const int & getId() const{return id;}

    const int & getId_pere() const{return id_pere;}

    void modifier_pere(CompteVirtuel* c){comptePere=c;}

    QString getNomCompte() const{return nomCompte;}

    virtual void majSolde(int debit, int credit)=0;

    virtual void majSolde(int difference)=0;

    virtual ~Compte();
};

class CompteVirtuel : public Compte{
protected:
    vector<Compte*> listeCompte;

public:
    friend class CompteReel;
    CompteVirtuel(const QString type, CompteVirtuel* cp, double s):Compte(type, cp, s){cp->listeCompte.push_back(this);}
    CompteVirtuel(const QString type, double s, double id1, double id_pere1, const QString nom):Compte(type, s, id1, id_pere1,nom){}
    CompteVirtuel(const QString type );
    Compte* createCompte(const QString compte, int solde);
    void ajouterCompte(Compte* c){listeCompte.push_back(c);}

    void majSolde(int difference);

    void majSolde(int debit, int credit){printf("erreur, fonction non disponible dans compte virtuel");}

    ~CompteVirtuel();
};

class CompteReel : public Compte{
private:
    vector<Transaction*> listeTrans;

public:
    friend class Transaction;
    friend class CompteVirtuel;
    CompteReel(const QString type, CompteVirtuel* cp, double s):Compte(type, cp, s){cp->listeCompte.push_back(this);}
    CompteReel(const QString type, double s, double id1, double id_pere1, const QString nom):Compte(type, s, id1, id_pere1,nom){}
    void majSolde(int difference){printf("erreur, fonction non disponible dans compte reel");}

    void majSolde(int debit, int credit);

    void createTrans(int date, QString refT, QString intitule, CompteReel* cible, int debit, int credit);
    void addTrans(Transaction* t){listeTrans.push_back(t);}
    ~CompteReel();
};

class CompteManager{
    public:
    vector<Compte*> tab;
public:
    CompteManager(){};

    vector<Compte*>::iterator begin(){return tab.begin();}
    vector<Compte*>::iterator end(){return tab.end();}

    vector<Compte*>::const_iterator begin() const {return tab.begin();}
    vector<Compte*>::const_iterator end() const {return tab.end();}
    void addCompte(Compte* c){
        tab.push_back(c);
    }
    int getsize(){return tab.size();}
    CompteManager& operator<<(Compte& c);
};

class Transaction{
private:
    int id; // commune à toute les transactions d'une même opération
    int dateT;
    QString referenceT;
    QString intituleT;
    CompteReel* cible;
    int id_cible;
    double debit;
    double credit;
    bool rapprochee;

public:
    Transaction(int date, QString refT, QString intitule, CompteReel* ci, int d, int c):dateT(date),referenceT(refT),intituleT(intitule),cible(ci){
        if(d!=0 && c !=0){
            int diff = d-c;

            if(diff>0){
                debit=diff;
                credit=0;
            }
            else{
                debit=0;
                credit=-diff;
            }
        }
        else{
            if(d<0){
                credit=-d;
                debit=0;
            }
            else{
                    if(c<0){
                        debit=-c;
                        credit=0;
                    }
                    else{
                        debit=d;
                        credit=c;
                    }
            }
        }
        ci->majSolde(debit,credit);
    }
    // constructeur pour la restauration
    Transaction(int date, QString refT, QString intitule, int d, int c, bool rapp, double id1, double id_cible1):dateT(date),referenceT(refT),intituleT(intitule),rapprochee(rapp), id(id1), id_cible(id_cible1), debit(d), credit(c){};
    const int & getId() const{return id;}
    const int & getId_cible() const{return id_cible;}
    const int & getDateT() const{return dateT;}
    const QString & getReferenceT() const{return referenceT;}
    const QString & getIntituleT() const{return intituleT;}
    const double & getDebit() const{return debit;}
    const double & getCredit() const{return credit;}
    const bool getRapprochee() const{return rapprochee;}
    const CompteReel* getCible() const{return cible;}
    void setToRapprochee(){rapprochee=true;}
    void changecible(CompteReel* c){cible=c;}
};

class TransactionManager{
    public:
    vector<Transaction*> tab;
public:
    TransactionManager(){};

    vector<Transaction*>::iterator begin(){return tab.begin();}
    vector<Transaction*>::iterator end(){return tab.end();}

    vector<Transaction*>::const_iterator begin() const {return tab.begin();}
    vector<Transaction*>::const_iterator end() const {return tab.end();}
    void addTrans(Transaction* c){
        tab.push_back(c);
    }
    int getsize(){return tab.size();}
    TransactionManager& operator<<(Transaction& c);
};

void rapprocher();

#endif // COMPTE_H_INCLUDED


#endif // COMPTE_H
