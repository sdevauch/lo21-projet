#ifndef SAUVEGARDE_H
#define SAUVEGARDE_H

#endif // SAUVEGARDE_H_INCLUDED
#include "compte.h"
#include <QApplication>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNodeList>
#include <QFile>
#include <QString>
#include <QFileDialog>
#include <QTextStream>
#include <string>
#include <iostream>

void restaurer(CompteManager* manager, TransactionManager* trans_manager){
// En premier lieux, nous effectuons la restauration des données de Comptes

    QString filename = QFileDialog::getOpenFileName(nullptr, "sauvegarde_compte.xml");


    QFile file(filename);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            std::cerr << "test" << std::endl;
            return;
        }
        else
        {
            std::cout << "=== Lecture du fichier " << "sauvegarde_compte.xml"
                      << " ===" << std::endl;
        }

        // création du document et parsing du fichier xml
        QDomDocument xmlDoc;
        xmlDoc.setContent(&file);

//on insère tout les éléments de type Compte dans un élément CompteManager

    vector<CompteVirtuel> liste_comptesVirtuels; // contiendra uniquement les comtpes virtuels
    vector<CompteReel> liste_comptesReels; // contiendra uniquement les comtpes virtuels

    QDomNodeList comptes = xmlDoc.elementsByTagName("compte");

    for (int i = 0; i < comptes.size(); i++)
    {
        QDomElement cptnode = comptes.item(i).toElement();
        QString virtuelreel=cptnode.attribute("Virtuel_reel", "");
        if(virtuelreel=="virtuel"){
            CompteVirtuel * c = new CompteVirtuel(cptnode.attribute("typeCompte", ""),cptnode.attribute("solde", "").toFloat(),cptnode.attribute("id", "").toUInt(),cptnode.attribute("id_pere", "").toUInt(),cptnode.attribute("nomCompte", ""));
            //penser à libérer l'espace à l'arrêt de l'application
            manager->addCompte(c);
            liste_comptesVirtuels.push_back(*c);
        }
        if(virtuelreel=="reel"){
            CompteReel * c = new CompteReel(cptnode.attribute("typeCompte"),cptnode.attribute("solde", "").toFloat(),cptnode.attribute("id", "").toUInt(),cptnode.attribute("id_pere", "").toUInt(),cptnode.attribute("nomCompte", ""));
            //penser à libérer l'espace à l'arrêt de l'application
            manager->addCompte(c);
            liste_comptesReels.push_back(*c);
        }
    }


    int n=liste_comptesVirtuels.size();



    for(int j=0; j<n; j++){
       int id_pere=manager->tab[j]->getId_pere();
       if(id_pere==0){
            manager->tab[j]->modifier_pere(nullptr);
       }
       else{
            int k=0;
            CompteVirtuel* c=&liste_comptesVirtuels[k];
            while(k<n && c->getId()!=id_pere){
                k+=1;
                c=&liste_comptesVirtuels[k];
            }
            manager->tab[j]->modifier_pere(c);
            c->ajouterCompte(manager->tab[j]);
       }
    }




filename = QFileDialog::getOpenFileName(nullptr, "sauvegarde_transactions.xml");
QFile file1(filename);
    if (!file1.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        std::cerr << "Erreur de lecture du fichier !" << std::endl;
        return;
    }
    else
    {
        std::cout << "=== Lecture du fichier " << "sauvegarde_transactions.xml"
                  << " ===" << std::endl;
    }

    // création du document et parsing du fichier xml
    QDomDocument xmlDoc1;
    xmlDoc1.setContent(&file1);


    QDomNodeList transaction = xmlDoc1.elementsByTagName("transaction");

    for (int i = 0; i < transaction.size(); i++)
    {
        QDomElement tranode = transaction.item(i).toElement();
        Transaction *t = new Transaction(tranode.attribute("date", "").toUInt(), tranode.attribute("referenceT", ""), tranode.attribute("intituleT", ""), tranode.attribute("debit", "").toDouble(), tranode.attribute("credit","").toDouble(), (tranode.attribute("rapprochee", "")=="True"), tranode.attribute("id", "").toUInt(), tranode.attribute("id_compte", "").toUInt());
        trans_manager->addTrans(t);
    }

//Maintenant, on parcour la liste des transactions et on attribue à chacune le compte associé

    cout<<"size transM : "<<trans_manager->getsize()<<"\n";


for(int e=0; e<trans_manager->getsize(); e++){
    Transaction* t=trans_manager->tab[e];
    unsigned int l=0;
    if(liste_comptesReels.size()>0){
        CompteReel* cr=&liste_comptesReels[l];
        //cout<<cr->getId()<<"\n";
        while(cr->getId()!=t->getId_cible() && l<liste_comptesReels.size()){
            l++;
            cr=&liste_comptesReels[l];
        }
        cr->addTrans(t);
        t->changecible(cr);
    }

}



}





void save_new_transaction(Transaction* t){
    QDomDocument doc;
    QDomElement root = doc.createElement("transaction");
    doc.appendChild(root);


    // on crée un élément global pour le compte : le nom donné ici à
    // l'élément correspond au nom du tag dans la balise
    QDomElement traElement = doc.createElement("transaction");

    // on ajoute simplement les attributs sur l'élément avec la méthode
    // setAttribute
    // on remarquera que l'ordre d'ajout des attributs n'est pas
    // nécessairement le même que l'ordre des lignes qui suivent
    traElement.setAttribute("rapprochee", t->getRapprochee());
    traElement.setAttribute("credit", QString::number(t->getCredit()));
    traElement.setAttribute("debit", QString::number(t->getDebit()));
    traElement.setAttribute("intituleT", t->getIntituleT());
    traElement.setAttribute("reference", t->getReferenceT());
    traElement.setAttribute("date", QString::number(t->getDateT()));
    traElement.setAttribute("id_compte", QString::number(t->getId_cible()));
    traElement.setAttribute("id_compte", QString::number(t->getId()));

        // on ajoute le compte à l'élément principal
    root.appendChild(traElement);

    // ouverture du fichier en écriture
    QFile outfile("sauvegarde_transaction.xml");

    if (!outfile.open(QIODevice::ReadWrite))
        return;

    // création d'un flux pour gérer la sortie
    QTextStream stream(&outfile);

    // export du document
    stream << doc.toString();

    // fermeture du fichier
    outfile.close();
}

void sauvegardeTransaction(TransactionManager* TransM){
    QDomDocument doc;
    QDomElement root = doc.createElement("liste_transactions");
    doc.appendChild(root);
    QString filename = QFileDialog::getOpenFileName(nullptr, "sauvegarde_transactions.xml");
    for(int i=0; i<TransM->getsize(); i++){
        QDomElement traElement = doc.createElement("transaction");
        traElement.setAttribute("rapprochee", TransM->tab[i]->getRapprochee());
        traElement.setAttribute("credit", QString::number(TransM->tab[i]->getCredit()));
        traElement.setAttribute("debit", QString::number(TransM->tab[i]->getDebit()));
        traElement.setAttribute("intituleT", TransM->tab[i]->getIntituleT());
        traElement.setAttribute("reference", TransM->tab[i]->getReferenceT());
        traElement.setAttribute("date", QString::number(TransM->tab[i]->getDateT()));
        traElement.setAttribute("id_compte", QString::number(TransM->tab[i]->getId_cible()));
        traElement.setAttribute("id_compte", QString::number(TransM->tab[i]->getId()));
        root.appendChild(traElement);
    }

    QFile outfile(filename);

    if (!outfile.open(QIODevice::ReadWrite))
        return;

    // création d'un flux pour gérer la sortie
    QTextStream stream(&outfile);

    // export du document
    stream << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?> \n"+doc.toString();

    // fermeture du fichier
    outfile.close();
}



void sauvegarderComptes(CompteManager* CompteM){
    QDomDocument doc;
    QDomElement root = doc.createElement("liste_compte");
    doc.appendChild(root);
    QString filename = QFileDialog::getOpenFileName(nullptr, "sauvegarde_compte.xml");

    for(int i=0; i<CompteM->getsize();i++){
        QDomElement cptElement=doc.createElement("compte");
        CompteReel* cr=dynamic_cast<CompteReel*>(CompteM->tab[i]);
        if(cr){
            cptElement.setAttribute("id", cr->getId());
            cptElement.setAttribute("nomCompte", cr->getNomCompte());
            cptElement.setAttribute("typeCompte", cr->getTypeCompte());
            cptElement.setAttribute("solde", QString::number((cr->getSolde())));
            cptElement.setAttribute("id_pere", QString::number(cr->getId_pere()));
            cptElement.setAttribute("Virtuel_reel", "reel");
        }
        CompteVirtuel* cv=dynamic_cast<CompteVirtuel*>(CompteM->tab[i]);
        if(cv){
            cptElement.setAttribute("id", cv->getId());
            cptElement.setAttribute("nomCompte", cv->getNomCompte());
            cptElement.setAttribute("typeCompte", cv->getTypeCompte());
            cptElement.setAttribute("solde", QString::number((cv->getSolde())));
            cptElement.setAttribute("id_pere", QString::number(cv->getId_pere()));
            cptElement.setAttribute("Virtuel_reel", "virtuel");
        }
        std::cout<<"Sauvegarde d'un compte";
        root.appendChild(cptElement);

    }

    QFile outfile(filename);

    if (!outfile.open(QIODevice::ReadWrite))
        return;

    // création d'un flux pour gérer la sortie
    QTextStream stream(&outfile);

    // export du document
    stream << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?> \n"+doc.toString();

    // fermeture du fichier
    outfile.close();
}











